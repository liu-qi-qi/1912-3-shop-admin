import {
  LoginForm,
  ProFormText,
} from '@ant-design/pro-components'
import { useState } from 'react'
import style from './index.less'
import { v4 as uuidv4 } from 'uuid';
import loginimg from './logo.png'
const index = () => {
  const [codeImg,setcodeImg] = useState(()=>uuidv4())
  const [srclogo,getsrclogo] = useState(() => 
  `https://bjwz.bwie.com/mall4w/captcha.jpg?uuid=${codeImg}`
)
  const getcode = () => {
    setcodeImg(() => uuidv4())
    getsrclogo(() => `https://bjwz.bwie.com/mall4w/captcha.jpg?uuid=${codeImg}`)
  }
  const getuser=async (values:any) =>{
    console.log(values,'点击登录');
  }
  return (
  <div className={style.login_login}>
    <div className={style.login}>
      <LoginForm
    logo={loginimg}
    onFinish={getuser}
    title="八维电商平台"
    subTitle="三组专用后台管理系统"
    >
   <ProFormText
              name="username"
              placeholder={'账号'}
              className={style.username}
              rules={[
                {
                  required: true,
                  message: '用户名不能为空!',
                },
              ]}
            />
            <ProFormText.Password
              name="password"
              placeholder={'密码'}
              rules={[
                {
                  required: true,
                  message: '密码不能为空！',
                },
              ]}
            />
            <div className={style.login_code}>
              <ProFormText
               name="code"
              placeholder={'验证码'}></ProFormText>
              <div className={style.login_img}>
               <img src={srclogo}  onClick={getcode}/>
              </div>
            </div>
    </LoginForm>
  </div>
  </div>
  )
}

export default index

