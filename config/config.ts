import { defineConfig } from 'umi';
import routes from './routes'

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes,
  title:'三组电商',
  layout:{},
  fastRefresh: {},
}); 